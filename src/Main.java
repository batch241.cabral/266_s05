import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Erven Joshua", "0968-570-6772", "Aguinaldo");
        Contact contact2 = new Contact("Erven Joshua2", "0968-570-6773", "Aguinaldo2");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        ArrayList<Contact> contacts = phonebook.getContacts();
        if (contacts.isEmpty()) {
            System.out.println("The phonebook is empty!");
        } else {
            for (Contact contact : contacts) {
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println();
            }
        }

    }
}